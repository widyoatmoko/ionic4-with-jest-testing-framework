module.exports = {
  preset: 'jest-preset-angular',
  transformIgnorePatterns: [
    '<rootDir>/node_modules/(?!@ionic)',
  ],
  testPathIgnorePatterns: [
    '<rootDir>/cypress/'
  ],
};
