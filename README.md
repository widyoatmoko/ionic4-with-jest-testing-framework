# Ionic4 With Jest Testing Framework

## Setting Up Jest from Ionic Starter App

1. Remove Karma related stuff:
```
npm remove karma karma-chrome-launcher karma-coverage-istanbul-reporter karma-jasmine karma-jasmine-html-reporter
```
```
rm src/karma.conf.js src/test.ts
```
2. Install @angular-builders/jest and jest:
```
npm i -D jest @angular-builders/jest @types/jest
```
3. Update your Typescript configurations:
```
// src/tsconfig.spec.json
{
  "extends": "../tsconfig.json",
  "compilerOptions": {
    "outDir": "../out-tsc/spec",
    "module": "commonjs",
    "allowJs": true,
    "types": ["jest"]
  },
  "files": [
    "zone-flags.ts",
    "polyfills.ts"
  ],
  "include": [
    "**/*.spec.ts",
    "**/*.d.ts"
  ]
}
```
```
// tsconfig.json
"compilerOptions": {
	...
	"types": ["jest"]
}
```
4. In angular.json change @angular-devkit/build-angular:karma to @angular-builders/jest:run :
```
"projects": {
    ...
    "[your-project]": {
         ...
         "architect": {
                ...
                "test": {
                          "builder": "@angular-builders/jest:run"
                          "options": {
                                ... //see here
                          }
```
5. Add jest config:
```javascript
// src/jest.config.js
module.exports = {
  preset: 'jest-preset-angular',
  transformIgnorePatterns: [
    '<rootDir>/node_modules/(?!@ionic)',
  ],
  testPathIgnorePatterns: [
    '<rootDir>/cypress/'
  ],
};
```
6. Update app component test:
```typescript
// src/app/app.component.spec.ts
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, async } from '@angular/core/testing';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';

describe('AppComponent', () => {

  let styleDefault, hide, platformReadySpy, platformSpy;

  beforeEach(async(() => {
    styleDefault = jest.fn();
    hide = jest.fn();
    platformReadySpy = jest.fn(() => Promise.resolve());
    platformSpy = { ready: platformReadySpy };

    TestBed.configureTestingModule({
      declarations: [AppComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: StatusBar, useValue: { styleDefault }},
        { provide: SplashScreen, useValue: { hide }},
        { provide: Platform, useValue: platformSpy },
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should initialize the app', async () => {
    TestBed.createComponent(AppComponent);
    expect(platformSpy.ready).toHaveBeenCalled();
    await platformReadySpy;
    expect(styleDefault).toHaveBeenCalled();
    expect(hide).toHaveBeenCalled();
  });

  // TODO: add more tests!

});
```

## Running Test
```
ng test
```